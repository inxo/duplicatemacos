//
//  ViewController.swift
//  TestMac
//
//  Copyright © 2019 Test. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    var home = FileManager.default.homeDirectoryForCurrentUser
    let operationQueue = OperationQueue()
    
    @IBOutlet var start: NSButtonCell!
    @IBOutlet var resultText: NSTextView!
    @IBOutlet var result: NSScrollView!
    @IBOutlet var progress: NSProgressIndicator!
    
    @IBAction func search(_ sender: Any) {
        if (operationQueue.isSuspended || operationQueue.operationCount > 0) {
            operationQueue.isSuspended = false
            operationQueue.cancelAllOperations()
        }
        else {
            operationQueue.addOperation(SearchOperation(self))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        operationQueue.name = "search"
        operationQueue.qualityOfService = .background
        operationQueue.maxConcurrentOperationCount = 1
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    func startSearchStatus() {
        DispatchQueue.main.sync {
            progress.startAnimation(self)
            start.title = "Stop Search"
            
        }
    }
    
    func stopSearchStatus() {
        DispatchQueue.main.sync{
            progress.stopAnimation(self)
            start.title = "Start Search"
        }
    }
    var updateInProgress = false;
    
    func updateResult(_ results: [(key: Int, value: [String])]) {
        if (!updateInProgress) {
            updateInProgress = true;
            
            var result: String = ""
            for hashes in results {
                var i = 0
                for file in hashes.value {
                    if (i == 0) {
                        result.append(contentsOf: String(format: "%d files %@\n", hashes.value.count-1, file))
                    }
                    else {
                        result.append(contentsOf: file)
                        result.append(contentsOf: "\n")
                    }
                    i += 1
                }
            }
            
            DispatchQueue.main.sync {
                insertText(result)
                updateInProgress = false
            }
        }
    }
    
    private func insertText(_ text: String) {
        resultText.selectAll(self)
        resultText.delete(self)
        self.result.documentView!.insertText(text)
    }
}
