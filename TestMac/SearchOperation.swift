//
//  SearchOperation.swift
//  TestMac
//
//  Copyright © 2019 Test. All rights reserved.
//

import Foundation
import FileMD5Hash

class SearchOperation: Operation {
    var dic: [String:[String]] = [:]
    var equals: [String: Int] = [:]
    var result: [Int:[String]] = [:]
    
    let fileManager = FileManager.default;
    
    let viewController : ViewController
    
    init(_ viewController: ViewController) {
        self.viewController = viewController
    }
    
    override func main() {
        viewController.startSearchStatus()
        checkFolder(folder: viewController.home)
        viewController.stopSearchStatus()
    }
    
    private func checkFolder(folder: URL) {
        let urls = contentsOf(folder: folder)
        urls.forEach { (url) in
            if (isCancelled) {
                return
            }
            var isDir: ObjCBool = false;
            if(fileManager.fileExists(atPath: url.path, isDirectory: &isDir) && isDir.boolValue) {
                checkFolder(folder: url)
            }
            else{
                findDuplicate(file: url)
            }
        }
    }
    
    private func findDuplicate(file: URL) {
        let hash = FileHash.md5HashOfFile(atPath: file.path) as String
        if((hash) != "") {
            if(dic.index(forKey: hash) != nil){
                // check content foreach
                for dic_url in dic[hash]! {
                    if(FileManager.default.contentsEqual(atPath: file.path, andPath: dic_url)){
                        // add to equals
                        if(equals.index(forKey: hash) == nil){
                            let index = equals.count
                            equals[hash] = index
                            // add file size first in dictonary
                            let size = detectSize(file: dic_url)
                            result[index] = []
                            result[index]?.append(size)
                            result[index]?.append(dic_url)
                        }
                        result[equals[hash]!]?.append(file.path)
                        let resultOrdered = result.sorted {$0.key < $1.key}
                        viewController.updateResult(resultOrdered)
                        break
                    }
                }
            }
            
            dic[hash] = []
            dic[hash]?.append(file.path)
        }
    }
    
    private func detectSize(file: String) -> String {
        let fileAttributes = try! fileManager.attributesOfItem(atPath: file)
        let fileSize = fileAttributes[FileAttributeKey.size]
        let size = self.covertToFileString(with: (fileSize as! NSNumber).uint64Value)
        return size
    }
    
    private func contentsOf(folder: URL) -> [URL] {
        let fileManager = FileManager.default
        do {
            let contents = try fileManager.contentsOfDirectory(atPath: folder.path)
            let urls = contents.map {
                return folder.appendingPathComponent($0)
            }
            return urls
        } catch {
            return []
        }
    }
    
    private func covertToFileString(with size: UInt64) -> String {
        var value: Int = Int(size)
        var multiplyFactor = 0
        let tokens = ["bytes", "KB", "MB", "GB", "TB", "PB",  "EB",  "ZB", "YB"]
        while value > 1024 {
            value /= 1024
            value += 1
            multiplyFactor += 1
        }
        return String(format: "%d %@", value, tokens[multiplyFactor])
    }
    
}
